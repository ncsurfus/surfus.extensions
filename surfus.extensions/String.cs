﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Surfus.Extensions
{
    public static class StringExtensions
    {
        public static string Summarize(this string Source, char Trim)
        {
            char[] NewString = new char[Source.Length];
            bool FoundTrimChar = false;
            int newLength = 0;

            for (int i = 0; i < Source.Length; i++)
            {
                if (Source[i] != Trim)
                {
                    NewString[newLength] = Source[i];
                    newLength++;
                    FoundTrimChar = false;
                }
                else
                {
                    if (FoundTrimChar == false)
                    {
                        NewString[newLength] = Source[i];
                        newLength++;
                        FoundTrimChar = true;
                    }
                }
            }
            return new string(NewString, 0, newLength);
        }

        public static string Summarize(this string Source, string Trim)
        {
            char[] NewString = new char[Source.Length];
            bool FoundTrimChar = false;
            int newLength = 0;
            int i = 0;

            while (i < Source.Length - Trim.Length)
            {
                if (!Source.Contains(i, Trim.Length, Trim))
                {
                    NewString[newLength] = Source[i];
                    newLength++;
                    FoundTrimChar = false;
                    i++;
                }
                else
                {
                    if (FoundTrimChar == false)
                    {
                        for (int j = 0; j != Trim.Length; j++)
                        {
                            NewString[newLength] = Source[i];
                            newLength++;
                            i++;
                        }
                        FoundTrimChar = true;
                    }
                    else
                    {
                        i += Trim.Length;
                    }
                }
            }

            return new string(NewString, 0, newLength);
        }

        public static bool Contains(this string Source, int Start, int Length, string Trim)
        {
            bool MisMatch = false;
            if (Trim.Length == Length && Start + Length <= Source.Length)
            {
                for (int i = 0; i != Length; i++)
                {
                    if (Source[Start + i] != Trim[i])
                    {
                        MisMatch = true;
                    }
                }
            }
            return !MisMatch;
        }

        public static string getDigits(this string Source)
        {
            char[] Result = new char[Source.Length];
            int iResult = 0;

            for(int i = 0; i != Source.Length; i++)
            {
                if(Char.IsDigit(Source[i]))
                {
                    Result[iResult] = Source[i];
                    iResult++;
                }
            }
            return new string(Result, 0, iResult);
        }

        public static bool EndsWithAny(this string Source, IEnumerable<string> values, StringComparison Culture = StringComparison.CurrentCulture)
        {
            foreach(string value in values)
            {
                if (Source.EndsWith(value, Culture))
                {
                    return true;
                }
            }
            return false;
        }
    }
}