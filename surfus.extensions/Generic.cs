﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Surfus.Extensions
{
    public static class GenericExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
            {
                action(item);
            }
        }
    }
}
